#!/usr/bin/bash

# Variables
BUILD_DIR="/tmp/build"
REPO_PATH="/home/pingu/repo/hai-repo/x86_64"
AUR_URL="https://aur.archlinux.org"
PKGS_LIST=(hyprland-git hyprlang-git xdg-desktop-portal-hyprland-git hyprcursor-git hyprutils-git hyprlock-git)

mkdir -p /tmp/build
mkdir -p /home/pingu/repo/hai-repo/x86_64

for PKG in "${PKGS_LIST[@]}"
do
  git clone "$AUR_URL/$PKG.git" "$BUILD_DIR/$PKG"
  cd "$BUILD_DIR/$PKG" && makepkg -s --sign --needed
  # Remove the old pkg before adding the new one
  rm "$REPO_PATH/$PKG"*
  mv *.pkg.tar.zst* "$REPO_PATH"
  cd "$BUILD_DIR" && rm -rf "$PKG"
done

find "$REPO_PATH" -type f -name "*debug*" -exec rm {} \;

# Rebuild repo database
cd "$REPO_PATH"
rm -f elissa*
repo-add --verify --sign -R elissa.db.tar.gz *.pkg.tar.zst

# Remove symlinks
rm elissa.db
rm elissa.db.sig
rm elissa.files
rm elissa.files.sig

# Rename archives without the file ext
mv elissa.db.tar.gz elissa.db
mv elissa.db.tar.gz.sig elissa.db.sig
mv elissa.files.tar.gz elissa.files
mv elissa.files.tar.gz.sig elissa.files.sig

# git
git add -u
git add *
git commit -m "Update packages"
git push

echo "Done!"
